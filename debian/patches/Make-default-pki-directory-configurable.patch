From: Benjamin Drung <benjamin.drung@ionos.com>
Date: Thu, 13 Jan 2022 21:24:47 +0100
Subject: Make default pki directory configurable

The files in `/etc/salt/pki` are not configuration files in the sense of
the FHS ("local file used to control the operation of a program").
Debian wants to change the default location to `/var/lib/salt/pki` (to
properly follow FHS and to allow setting StateDirectory in the salt
master systemd configuration).

Therefore introduce a `STATE_DIR` syspaths variable which defaults to
`CONFIG_DIR`, but can be individually customized.

fixes #3396
Bug-Debian: https://bugs.debian.org/698898
Forwarded: https://github.com/saltstack/salt/pull/61453
Signed-off-by: Benjamin Drung <benjamin.drung@ionos.com>
---
 changelog/3396.added              |  5 +++++
 doc/ref/configuration/master.rst  |  2 +-
 doc/ref/configuration/minion.rst  |  2 +-
 doc/topics/installation/index.rst | 38 ++++++++++++++++++++++++++++++++++++++
 salt/config/__init__.py           |  6 +++---
 salt/syspaths.py                  |  5 +++++
 setup.py                          |  8 ++++++++
 7 files changed, 61 insertions(+), 5 deletions(-)
 create mode 100644 changelog/3396.added

diff --git a/changelog/3396.added b/changelog/3396.added
new file mode 100644
index 0000000..4e73ae1
--- /dev/null
+++ b/changelog/3396.added
@@ -0,0 +1,5 @@
+Introduce a `STATE_DIR` syspaths variable which defaults to `CONFIG_DIR`, but
+can be individually customized during installation by specifying
+`--salt-state-dir` during installation. Change the default `pki_dir` to
+`<STATE_DIR>/pki/master` (for the master) and `<STATE_DIR>/pki/minion` (for
+the minion).
diff --git a/doc/ref/configuration/master.rst b/doc/ref/configuration/master.rst
index e4d64f0..b8d3f7d 100644
--- a/doc/ref/configuration/master.rst
+++ b/doc/ref/configuration/master.rst
@@ -180,7 +180,7 @@ The path to the master's configuration file.
 ``pki_dir``
 -----------
 
-Default: ``/etc/salt/pki/master``
+Default: ``<STATE_DIR>/pki/master``
 
 The directory to store the pki authentication keys.
 
diff --git a/doc/ref/configuration/minion.rst b/doc/ref/configuration/minion.rst
index dcf6a31..782dce0 100644
--- a/doc/ref/configuration/minion.rst
+++ b/doc/ref/configuration/minion.rst
@@ -595,7 +595,7 @@ The path to the minion's configuration file.
 ``pki_dir``
 -----------
 
-Default: ``/etc/salt/pki/minion``
+Default: ``<STATE_DIR>/pki/minion``
 
 The directory used to store the minion's public and private keys.
 
diff --git a/doc/topics/installation/index.rst b/doc/topics/installation/index.rst
index 32d6f94..02b5230 100644
--- a/doc/topics/installation/index.rst
+++ b/doc/topics/installation/index.rst
@@ -146,6 +146,44 @@ provided like:
     minimal installation, you might need to install some additional packages
     from your OS vendor.
 
+Default directories
+-------------------
+
+During installation several default directories can be configured:
+
+======================  ============================
+variable                setup.py config option
+======================  ============================
+ROOT_DIR                --salt-root-dir
+SHARE_DIR               --salt-share-dir
+CONFIG_DIR              --salt-config-dir
+CACHE_DIR               --salt-cache-dir
+SOCK_DIR                --salt-sock-dir
+SRV_ROOT_DIR            --salt-srv-root-dir
+STATE_DIR               --salt-state-dir
+BASE_FILE_ROOTS_DIR     --salt-base-file-roots-dir
+BASE_PILLAR_ROOTS_DIR   --salt-base-pillar-roots-dir
+BASE_MASTER_ROOTS_DIR   --salt-base-master-roots-dir
+LOGS_DIR                --salt-logs-dir
+PIDFILE_DIR             --salt-pidfile-dir
+SPM_FORMULA_PATH        --salt-spm-formula-dir
+SPM_PILLAR_PATH         --salt-spm-pillar-dir
+SPM_REACTOR_PATH        --salt-spm-reactor-dir
+HOME_DIR                --salt-home-dir
+======================  ============================
+
+By default, salt tries to determine useful default values for the directories.
+You can override them during installation by specifying the config option. E.g.
+to change `STATE_DIR` from the default `/etc/salt` to `/var/lib/salt`, specify
+`--salt-state-dir` during installation:
+
+.. code-block:: bash
+
+  python setup.py --salt-state-dir=/var/lib/salt install
+
+All the variables listed in the table above are stored in `salt/_syspaths.py`
+during installation.
+
 Optional Dependencies
 ---------------------
 
diff --git a/salt/config/__init__.py b/salt/config/__init__.py
index 97a7fce..f160215 100644
--- a/salt/config/__init__.py
+++ b/salt/config/__init__.py
@@ -984,7 +984,7 @@ DEFAULT_MINION_OPTS = immutabletypes.freeze(
         "syndic_finger": "",
         "user": salt.utils.user.get_user(),
         "root_dir": salt.syspaths.ROOT_DIR,
-        "pki_dir": os.path.join(salt.syspaths.CONFIG_DIR, "pki", "minion"),
+        "pki_dir": os.path.join(salt.syspaths.STATE_DIR, "pki", "minion"),
         "id": "",
         "id_function": {},
         "cachedir": os.path.join(salt.syspaths.CACHE_DIR, "minion"),
@@ -1279,7 +1279,7 @@ DEFAULT_MASTER_OPTS = immutabletypes.freeze(
         "keep_jobs": 24,
         "archive_jobs": False,
         "root_dir": salt.syspaths.ROOT_DIR,
-        "pki_dir": os.path.join(salt.syspaths.CONFIG_DIR, "pki", "master"),
+        "pki_dir": os.path.join(salt.syspaths.STATE_DIR, "pki", "master"),
         "key_cache": "",
         "cachedir": os.path.join(salt.syspaths.CACHE_DIR, "master"),
         "file_roots": {
@@ -1628,7 +1628,7 @@ DEFAULT_PROXY_MINION_OPTS = immutabletypes.freeze(
         "proxy_always_alive": True,
         "proxy_keep_alive": True,  # by default will try to keep alive the connection
         "proxy_keep_alive_interval": 1,  # frequency of the proxy keepalive in minutes
-        "pki_dir": os.path.join(salt.syspaths.CONFIG_DIR, "pki", "proxy"),
+        "pki_dir": os.path.join(salt.syspaths.STATE_DIR, "pki", "proxy"),
         "cachedir": os.path.join(salt.syspaths.CACHE_DIR, "proxy"),
         "sock_dir": os.path.join(salt.syspaths.SOCK_DIR, "proxy"),
     }
diff --git a/salt/syspaths.py b/salt/syspaths.py
index 9e259cf..917a02c 100644
--- a/salt/syspaths.py
+++ b/salt/syspaths.py
@@ -47,6 +47,7 @@ EXPECTED_VARIABLES = (
     "SPM_PILLAR_PATH",
     "SPM_REACTOR_PATH",
     "SHARE_DIR",
+    "STATE_DIR",
 )
 
 try:
@@ -177,6 +178,10 @@ SRV_ROOT_DIR = __generated_syspaths.SRV_ROOT_DIR
 if SRV_ROOT_DIR is None:
     SRV_ROOT_DIR = os.path.join(ROOT_DIR, "srv")
 
+STATE_DIR = __generated_syspaths.STATE_DIR
+if STATE_DIR is None:
+    STATE_DIR = CONFIG_DIR
+
 BASE_FILE_ROOTS_DIR = __generated_syspaths.BASE_FILE_ROOTS_DIR
 if BASE_FILE_ROOTS_DIR is None:
     BASE_FILE_ROOTS_DIR = os.path.join(SRV_ROOT_DIR, "salt")
diff --git a/setup.py b/setup.py
index af8e448..e3234bf 100755
--- a/setup.py
+++ b/setup.py
@@ -349,6 +349,7 @@ class GenerateSaltSyspaths(Command):
                 cache_dir=self.distribution.salt_cache_dir,
                 sock_dir=self.distribution.salt_sock_dir,
                 srv_root_dir=self.distribution.salt_srv_root_dir,
+                state_dir=self.distribution.salt_state_dir,
                 base_file_roots_dir=self.distribution.salt_base_file_roots_dir,
                 base_pillar_roots_dir=self.distribution.salt_base_pillar_roots_dir,
                 base_master_roots_dir=self.distribution.salt_base_master_roots_dir,
@@ -746,6 +747,7 @@ CONFIG_DIR = {config_dir!r}
 CACHE_DIR = {cache_dir!r}
 SOCK_DIR = {sock_dir!r}
 SRV_ROOT_DIR= {srv_root_dir!r}
+STATE_DIR = {state_dir!r}
 BASE_FILE_ROOTS_DIR = {base_file_roots_dir!r}
 BASE_PILLAR_ROOTS_DIR = {base_pillar_roots_dir!r}
 BASE_MASTER_ROOTS_DIR = {base_master_roots_dir!r}
@@ -945,6 +947,11 @@ class SaltDistribution(distutils.dist.Distribution):
             ("salt-cache-dir=", None, "Salt's pre-configured cache directory"),
             ("salt-sock-dir=", None, "Salt's pre-configured socket directory"),
             ("salt-srv-root-dir=", None, "Salt's pre-configured service directory"),
+            (
+                "salt-state-dir=",
+                None,
+                "Salt's pre-configured variable state directory (used for storing pki data)",
+            ),
             (
                 "salt-base-file-roots-dir=",
                 None,
@@ -994,6 +1001,7 @@ class SaltDistribution(distutils.dist.Distribution):
         self.salt_cache_dir = None
         self.salt_sock_dir = None
         self.salt_srv_root_dir = None
+        self.salt_state_dir = None
         self.salt_base_file_roots_dir = None
         self.salt_base_thorium_roots_dir = None
         self.salt_base_pillar_roots_dir = None
